/*
	Deymeon's edit to make table re runnable
    Create the tblStudent Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblStudent`;
CREATE TABLE `tblStudent` (
    `intStudentID` INT NOT NULL AUTO_INCREMENT,
    `strFName` VARCHAR (25),
    `strLName` VARCHAR (25),
    PRIMARY KEY (`intStudentID`),
    UNIQUE INDEX `intStudentID_UNIQUE` (`intStudentID` ASC)
);