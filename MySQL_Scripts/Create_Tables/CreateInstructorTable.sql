/*
	Deymeon's edit to make table re runnable
    Create the tblInstructor Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblInstructor`;
CREATE TABLE `tblInstructor` (
    `intInstructorID` INT NOT NULL AUTO_INCREMENT,
    `strFName` VARCHAR (25),
    `strLName` VARCHAR (25),
    PRIMARY KEY (`intInstructorID`),
    UNIQUE INDEX `intInstructorID_UNIQUE` (`intInstructorID`  ASC)
);