/*
	Deymeon's edit to make table re runnable
    Create the tblCourses Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblCourse`;
CREATE TABLE `tblCourse` (
    `intCourseID` INT NOT NULL AUTO_INCREMENT,
    `strCourseName` VARCHAR(50) NOT NULL,
    `strCourseDesc` VARCHAR(150) NULL,
	`intCreditHours` INT NOT NULL,
	`intInstructorID` INT NOT NULL,
	`bolAllowToEnroll` BOOLEAN NULL,
    PRIMARY KEY (`intCourseID`),
    UNIQUE INDEX `intCourseID_UNIQUE` (`intCourseID` ASC)
);