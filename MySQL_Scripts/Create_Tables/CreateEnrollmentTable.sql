/*
	Deymeon's edit to make table re runnable
	Create the tblEnrollment Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblEnrollment`;
CREATE TABLE `tblEnrollment` (
    `intEnrollmentID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `intStudentID` INT NOT NULL,
    `tsEnrollmentDate` timestamp,
    `bolStatus` boolean,
    PRIMARY KEY (`intEnrollmentID`),
    UNIQUE INDEX `intEnrollmentID_UNIQUE` (`intEnrollmentID` ASC)
);