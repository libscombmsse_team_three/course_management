/* 
	Deymeon's edit to make table re runnable
	Create tblSection Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblSection`;
CREATE TABLE `tblSection` (
    `intClassID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `intSectionID` INT NOT NULL,
    `intInstructorID` INT NOT NULL,
    `dtRegistrationCloses` datetime,
    PRIMARY KEY (`intClassID`),
    UNIQUE INDEX `intClassID_UNIQUE` (`intClassID` ASC)
);