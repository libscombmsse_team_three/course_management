/*
	Create the tblSection_Updated Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblSection_Updated`;
CREATE TABLE `tblSection_Updated` (
    `intSectionID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `strSectionDesc` VARCHAR(150) NULL,
    PRIMARY KEY (`intSectionID`),
    UNIQUE INDEX `intSectionID_UNIQUE` (`intSectionID` ASC)
);