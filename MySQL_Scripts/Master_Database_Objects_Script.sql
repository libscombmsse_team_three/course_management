Use Lipscomb; 
 
/*
	Deymeon's edit to make table re runnable
    Create the tblCourses Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblCourse`;
CREATE TABLE `tblCourse` (
    `intCourseID` INT NOT NULL AUTO_INCREMENT,
    `strCourseName` VARCHAR(50) NOT NULL,
    `strCourseDesc` VARCHAR(150) NULL,
	`intCreditHours` INT NOT NULL,
	`intInstructorID` INT NOT NULL,
	`bolAllowToEnroll` BOOLEAN NULL,
    PRIMARY KEY (`intCourseID`),
    UNIQUE INDEX `intCourseID_UNIQUE` (`intCourseID` ASC)
); 
 
/*
	Deymeon's edit to make table re runnable
	Create the tblEnrollment Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblEnrollment`;
CREATE TABLE `tblEnrollment` (
    `intEnrollmentID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `intStudentID` INT NOT NULL,
    `tsEnrollmentDate` timestamp,
    `bolStatus` boolean,
    PRIMARY KEY (`intEnrollmentID`),
    UNIQUE INDEX `intEnrollmentID_UNIQUE` (`intEnrollmentID` ASC)
); 
 
/*
	Deymeon's edit to make table re runnable
    Create the tblInstructor Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblInstructor`;
CREATE TABLE `tblInstructor` (
    `intInstructorID` INT NOT NULL AUTO_INCREMENT,
    `strFName` VARCHAR (25),
    `strLName` VARCHAR (25),
    PRIMARY KEY (`intInstructorID`),
    UNIQUE INDEX `intInstructorID_UNIQUE` (`intInstructorID`  ASC)
); 
 
/* 
	Deymeon's edit to make table re runnable
	Create tblSection Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblSection`;
CREATE TABLE `tblSection` (
    `intClassID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `intSectionID` INT NOT NULL,
    `intInstructorID` INT NOT NULL,
    `dtRegistrationCloses` datetime,
    PRIMARY KEY (`intClassID`),
    UNIQUE INDEX `intClassID_UNIQUE` (`intClassID` ASC)
); 
 
/*
	Create the tblSection_Updated Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblSection_Updated`;
CREATE TABLE `tblSection_Updated` (
    `intSectionID` INT NOT NULL AUTO_INCREMENT,
    `intCourseID` INT NOT NULL,
    `strSectionDesc` VARCHAR(150) NULL,
    PRIMARY KEY (`intSectionID`),
    UNIQUE INDEX `intSectionID_UNIQUE` (`intSectionID` ASC)
); 
 
/*
	Deymeon's edit to make table re runnable
    Create the tblStudent Table/Primary Key/Index
*/
DROP TABLE IF EXISTS `tblStudent`;
CREATE TABLE `tblStudent` (
    `intStudentID` INT NOT NULL AUTO_INCREMENT,
    `strFName` VARCHAR (25),
    `strLName` VARCHAR (25),
    PRIMARY KEY (`intStudentID`),
    UNIQUE INDEX `intStudentID_UNIQUE` (`intStudentID` ASC)
); 
 
/*
	Audit Table Creation
*/
Drop Table If Exists `tblAudit`;
Create Table `tblAudit` (
	`intAudID` INT NOT NULL AUTO_INCREMENT,
    `dtChangeDateTime` datetime Not Null,
    `strChangeType` varchar(50) Not NULL,
    `strTableName` varchar(200) Not Null,
    `strUniqueKey` varchar(50) Not Null,
    `strFieldName` varchar(200) Not Null,
    `strOldFieldValue` varchar(2000) Null,
    `strNewFieldValue` varchar(2000) Null,
    PRIMARY KEY (`intAudID`),
    UNIQUE INDEX `intAudID_UNIQUE` (`intAudID` ASC)
);

/* Define the Audit Delimiter */
Delimiter $$

/* Trigger Insert SP */
DROP procedure IF EXISTS sp_tblAuditInsert;$$
CREATE PROCEDURE sp_tblAuditInsert(
IN dtChangeDateTime DateTime,
In strChangeType varchar(50),
In strTableName varchar(200),
In strUniqueKey varchar(50),
In strFieldName varchar(200),
In strOldFieldValue varchar(2000),
In strNewFieldValue varchar(2000)
)
BEGIN
    Insert Into `tblAudit`
		Set `dtChangeDateTime` = dtChangeDateTime,
			`strChangeType` = strChangeType,
            `strTableName` = strTableName,
            `strUniqueKey` = strUniqueKey,
            `strFieldName` = strFieldName,
            `strOldFieldValue` = strOldFieldValue,
            `strNewFieldValue` = strNewFieldValue;
END$$

/* 
	 tblSection Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUSection`;$$
Create Trigger `trgUSection` AFTER UPDATE On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.intSectionID <> New.intSectionID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intSectionID',Old.intSectionID,New.intSectionID);
	End If;
    
    If Old.intInstructorID <> New.intInstructorID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intInstructorID',Old.intInstructorID,New.intInstructorID);
	End If;
    
    If Old.dtRegistrationCloses <> New.dtRegistrationCloses Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'dtRegistrationCloses',Old.dtRegistrationCloses,New.dtRegistrationCloses);
	End If;
End$$

Drop Trigger IF Exists `trgISection`;$$
Create Trigger `trgISection` AFTER INSERT On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intClassID',NULL,New.intClassID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intSectionID',NULL,New.intSectionID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'dtRegistrationCloses',NULL,New.dtRegistrationCloses);
End$$

Drop Trigger IF Exists `trgDSection`;$$
Create Trigger `trgDSection` AFTER Delete On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intClassID',Old.intClassID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intSectionID',Old.intSectionID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'dtRegistrationCloses',Old.dtRegistrationCloses,Null);
End$$

/* 
	 tblInstructor Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUInstructor`;$$
Create Trigger `trgUInstructor` AFTER UPDATE On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    If Old.strFName <> New.strFName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblInstructor',Old.intInstructorID,'strFName',Old.strFName,New.strFName);
	End If;
    
    If Old.strLName <> New.strLName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblInstructor',Old.intInstructorID,'strLName',Old.strFName,New.strFName);
	End If;
End$$

Drop Trigger IF Exists `trgIInstructor`;$$
Create Trigger `trgIInstructor` AFTER INSERT On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'strFName',NULL,New.strFName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'strLName',NULL,New.strLName);
End$$

Drop Trigger IF Exists `trgDInstructor`;$$
Create Trigger `trgDInstructor` AFTER Delete On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'strFName',Old.strFName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'strLName',Old.strLName,Null);
End$$

/* 
	 tblEnrollment Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUEnrollment`;$$
Create Trigger `trgUEnrollment` AFTER UPDATE On `tblEnrollment`
For Each Row Begin
    Declare dtNow DateTime;
    Set dtNow = Now();
    
    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollmentID',Old.intEnrollmentID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.intStudentID <> New.intStudentID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'intStudent',Old.intStudentID,New.intStudentID);
	End If;
    
    If Old.tsEnrollmentDate <> New.tsEnrollmentDate Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'tsEnrollmentDate',Old.tsEnrollmentDate,New.tsEnrollmentDate);
	End If;
    
    If Old.bolStatus <> New.bolStatus Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'bolStatus',Old.bolStatus,New.bolStatus);
	End If;
End$$

Drop Trigger IF Exists `trgIEnrollment`;$$
Create Trigger `trgIEnrollment` AFTER INSERT On `tblEnrollment`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'intStudentID',NULL,New.intStudentID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'tsEnrollmentDate',NULL,New.tsEnrollmentDate);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'bolStatus',NULL,New.bolStatus);
End$$

Drop Trigger IF Exists `trgDEnrollment`;$$
Create Trigger `trgDEnrollment` AFTER Delete On `tblEnrollment`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'intStudentID',Old.intStudentID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'tsEnrollmentDate',Old.tsEnrollmentDate,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'bolStatus',Old.bolStatus,Null);
End$$

/* 
	 tblCourse Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUCourse`;$$
Create Trigger `trgUCourse` AFTER UPDATE On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    
    If Old.strCourseName <> New.strCourseName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'strCourseName',Old.strCourseName,New.strCourseName);
	End If;
    
    If Old.strCourseDesc <> New.strCourseDesc Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'strCourseDesc',Old.strCourseDesc,New.strCourseDesc);
	End If;
    
    If Old.intCreditHours <> New.intCreditHours Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'intCreditHours',Old.intCreditHours,New.intCreditHours);
	End If;
    
    If Old.intInstructorID <> New.intInstructorID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'intInstructorID',Old.intInstructorID,New.intInstructorID);
	End If;
    
    If Old.bolAllowToEnroll <> New.bolAllowToEnroll Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'bolAllowToEnroll',Old.bolAllowToEnroll,New.bolAllowToEnroll);
	End If;
End$$

Drop Trigger IF Exists `trgICourse`;$$
Create Trigger `trgICourse` AFTER INSERT On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'strCourseName',NULL,New.strCourseName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'strCourseDesc',NULL,New.strCourseDesc);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intCreditHours',NULL,New.intCreditHours);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'bolAllowToEnroll',NULL,New.bolAllowToEnroll);
End$$

Drop Trigger IF Exists `trgDCourse`;$$
Create Trigger `trgDCourse` AFTER Delete On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'strCourseName',Old.strCourseName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'strCourseDesc',Old.strCourseDesc,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intCreditHours',Old.intCreditHours,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'bolAllowToEnroll',Old.bolAllowToEnroll,Null);
End$$

/* 
	 tblStudent Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUStudent`;$$
Create Trigger `trgUStudent` AFTER UPDATE On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    If Old.strFName <> New.strFName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblStudent',Old.intStudentID,'strFName',Old.strFName,New.strFName);
	End If;
    
    If Old.strLName <> New.strLName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblStudent',Old.intStudentID,'strLName',Old.strFName,New.strFName);
	End If;
End$$

Drop Trigger IF Exists `trgIStudent`;$$
Create Trigger `trgIStudent` AFTER INSERT On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'intStudentID',NULL,New.intStudentID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'strFName',NULL,New.strFName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'strLName',NULL,New.strLName);
End$$

Drop Trigger IF Exists `trgDStudent`;$$
Create Trigger `trgDStudent` AFTER Delete On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'intStudentID',Old.intStudentID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'strFName',Old.strFName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'strLName',Old.strLName,Null);
End$$

/* 
	 tblSection_Updated Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUSection_Updated`;$$
Create Trigger `trgUSection_Updated` AFTER UPDATE On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();

    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection_Updated',Old.intSectionID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.strSectionDesc <> New.strSectionDesc Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection_Updated',Old.intSectionID,'strSectionDesc',Old.strSectionDesc,New.strSectionDesc);
	End If;
End$$

Drop Trigger IF Exists `trgISection_Updated`;$$
Create Trigger `trgISection_Updated` AFTER INSERT On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'intSectionID',NULL,New.intSectionID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'strSectionDesc',NULL,New.strSectionDesc);
    
End$$

Drop Trigger IF Exists `trgDSection_Updated`;$$
Create Trigger `trgDSection_Updated` AFTER Delete On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'intSectionID',Old.intSectionID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'strSectionDesc',Old.strSectionDesc,Null);
End$$

Delimiter ; 
 
Delimiter $$
DROP procedure IF EXISTS sp_Section_Info;$$
CREATE PROCEDURE sp_Section_Info ()
BEGIN

  SELECT 
    tblSection.intClassID,
    tblSection.intCourseID,
    tblSection.intSectionID,
    tblSection.intInstructorID,
    tblSection.dtRegistrationCloses,
    tblInstructor.strFName,
    tblInstructor.strLName,
    tblCourse.strCourseName,
    tblCourse.strCourseDesc,
    tblCourse.intCreditHours
  FROM tblSection, tblInstructor, tblCourse
  where tblSection.intInstructorID=tblInstructor.intInstructorID 
        and tblSection.intCourseID=tblCourse.intCourseID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Delete;$$
CREATE PROCEDURE sp_tblCourse_Delete(
IN pintCourseID INT(11)

)
BEGIN
  DELETE FROM Lipscomb.tblCourse
  WHERE intCourseID = pintCourseID;

  select "Success";
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Insert;$$
CREATE PROCEDURE sp_tblCourse_Insert(
IN pstrCourseName VARCHAR(50),
IN pstrCourseDesc VARCHAR(150),
IN pintCreditHours  INT(11) 
)
BEGIN
    INSERT INTO Lipscomb.tblCourse(strCourseName,strCourseDesc,intCreditHours)
      VALUES(pstrCourseName,pstrCourseDesc,pintCreditHours);
      
	select "Success";
    
END$$

Delimiter ; 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Inst_select;$$
CREATE PROCEDURE sp_tblCourse_Inst_select ()
BEGIN
  SELECT * FROM tblCourse c 
	INNER JOIN tblInstructor i 
		ON c.intInstructorID=i.intInstructorID
	ORDER BY c.intCourseID ASC;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Update;$$
CREATE PROCEDURE sp_tblCourse_Update(
IN pintCourseID INT(11),
IN pstrCourseName VARCHAR(50),
IN pstrCourseDesc VARCHAR(150),
IN pintCreditHours  INT(11),
IN pbolAllowEnroll TINYINT(1),
IN pintInstructorID INT(11)
)
BEGIN
  UPDATE Lipscomb.tblCourse
  SET
    strCourseName = pstrCourseName,
    strCourseDesc =pstrCourseDesc,
    intCreditHours = pintCreditHours,
    bolAllowEnroll=pbolAllowEnroll,
    intInstructorID=pintInstructorID
  WHERE 
    intCourseID = pintCourseID;
      
	select "Success";
    
END$$

Delimiter ;






 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Delete;$$
CREATE PROCEDURE sp_tblEnrollment_Delete(
IN pintCourseID INT(11),
IN pintStudentID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

     DELETE FROM Lipscomb.tblEnrollment
     WHERE intCourseID = pintCourseID AND intStudentID = pintStudentID;

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Insert;$$
CREATE PROCEDURE sp_tblEnrollment_Insert(
IN pintCourseID INT(11),
IN pintStudentID INT(11) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    INSERT INTO Lipscomb.tblEnrollment(intCourseID,intStudentID,tsEnrollmentDate)
      VALUES(pintCourseID,pintStudentID,NOW());
      
	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_All;$$
CREATE PROCEDURE sp_tblEnrollment_Select_All ()
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_ClassID;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_ClassID (IN pintClassID INT(11))
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where intClassID=pintClassID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_EnrollmentDate;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_EnrollmentDate (IN ptsEnrollmentDate DATETIME)
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where tsEnrollmentDate=ptsEnrollmentDate;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_StudentID;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_StudentID (IN pintStudentID INT(11))
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where intStudentID=pintStudentID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Delete;$$
CREATE PROCEDURE sp_tblInstructor_Delete(
IN pintInstructorID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblInstructor
    WHERE intInstructorID = pintInstructorID;

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Insert;$$
CREATE PROCEDURE sp_tblInstructor_Insert(
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    INSERT INTO Lipscomb.tblInstructor(strFName,strLName)
    VALUES(pstrFName,pstrLName);
      
	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Select_All;$$
CREATE PROCEDURE sp_tblInstructor_Select_All ()
BEGIN
  SELECT 
    intInstructorID,
    strFName,
    strLName
FROM tblInstructor;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Select_by_InstructorID;$$
CREATE PROCEDURE sp_tblInstructor_Select_by_InstructorID (IN pintInstructorID INT(11))
BEGIN
  SELECT 
    intInstructorID,
    strFName,
    strLName
  FROM tblInstructor
  where intInstructorID=pintInstructorID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Update;$$
CREATE PROCEDURE sp_tblInstructor_Update(
IN pintInstructorID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblInstructor SET
      strFName = pstrFName,
      strLName = pstrLName
    WHERE `intInstructorID` = pintInstructorID;

      
	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Delete;$$
CREATE PROCEDURE sp_tblSection_Delete(
IN pintClassID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblSection
    WHERE intClassID = pintClassID;

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Insert;$$
CREATE PROCEDURE sp_tblSection_Insert(
IN pintCourseID INT(11),
IN pintSectionID INT(11), 
IN pintInstructorID INT(11) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    #intClassID auto increments & dtRegistrationCloses defaults to null
    INSERT INTO Lipscomb.tblSection(intCourseID,intSectionID,intInstructorID)
    VALUES(pintCourseID,pintSectionID,pintInstructorID);
      
	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_All;$$
CREATE PROCEDURE sp_tblSection_Select_All ()
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_CourseID;$$
CREATE PROCEDURE sp_tblSection_Select_by_CourseID (IN pintCourseID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intCourseID=pintCourseID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_InstructorID;$$
CREATE PROCEDURE sp_tblSection_Select_by_InstructorID (IN pintInstructorID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intInstructorID=pintInstructorID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_SectionID;$$
CREATE PROCEDURE sp_tblSection_Select_by_SectionID (IN pintSectionID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intSectionID=pintSectionID;

    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Update;$$
CREATE PROCEDURE sp_tblSection_Update(
IN pintClassID INT(11),
IN pintCourseID INT(11),
IN pintSectionID INT(11), 
IN pintInstructorID INT(11),
IN pdtRegistrationCloses DATETIME
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblSection SET 
      intCourseID =pintCourseID,
      intSectionID = pintSectionID,
      intInstructorID = pintInstructorID,
      dtRegistrationCloses = pdtRegistrationCloses
    WHERE intClassID = pintClassID;

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Delete;$$
CREATE PROCEDURE sp_tblStudent_Delete(
IN pntStudentID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblStudent
    WHERE ntStudentID=pntStudentID;

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Insert;$$
CREATE PROCEDURE sp_tblStudent_Insert(
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    INSERT INTO Lipscomb.tblStudent(strFName,strLName)
    VALUES(pstrFName,pstrLName);

	select strResponse;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Select_All;$$
CREATE PROCEDURE sp_tblStudent_Select_All ()
BEGIN
    SELECT tblStudent.intStudentID,
           tblStudent.strFName,
           tblStudent.strLName
    FROM Lipscomb.tblStudent;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Select_by_StudentID;$$
CREATE PROCEDURE sp_tblStudent_Select_by_StudentID (
IN pntStudentID INT(11)
)
BEGIN
    SELECT tblStudent.intStudentID,
           tblStudent.strFName,
           tblStudent.strLName
    FROM Lipscomb.tblStudent
    WHERE ntStudentID=pntStudentID;
    
END$$

Delimiter ;

 
 
Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Update;$$
CREATE PROCEDURE sp_tblStudent_Update(
IN pntStudentID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblStudent SET 
      strFName = pstrFName,
      strLName = pstrLName
    WHERE intStudentID =pntStudentID;

	select strResponse;
    
END$$

Delimiter ;

 
 
