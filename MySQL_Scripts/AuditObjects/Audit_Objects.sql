/*
	Audit Table Creation
*/
Drop Table If Exists `tblAudit`;
Create Table `tblAudit` (
	`intAudID` INT NOT NULL AUTO_INCREMENT,
    `dtChangeDateTime` datetime Not Null,
    `strChangeType` varchar(50) Not NULL,
    `strTableName` varchar(200) Not Null,
    `strUniqueKey` varchar(50) Not Null,
    `strFieldName` varchar(200) Not Null,
    `strOldFieldValue` varchar(2000) Null,
    `strNewFieldValue` varchar(2000) Null,
    PRIMARY KEY (`intAudID`),
    UNIQUE INDEX `intAudID_UNIQUE` (`intAudID` ASC)
);

/* Define the Audit Delimiter */
Delimiter $$

/* Trigger Insert SP */
DROP procedure IF EXISTS sp_tblAuditInsert;$$
CREATE PROCEDURE sp_tblAuditInsert(
IN dtChangeDateTime DateTime,
In strChangeType varchar(50),
In strTableName varchar(200),
In strUniqueKey varchar(50),
In strFieldName varchar(200),
In strOldFieldValue varchar(2000),
In strNewFieldValue varchar(2000)
)
BEGIN
    Insert Into `tblAudit`
		Set `dtChangeDateTime` = dtChangeDateTime,
			`strChangeType` = strChangeType,
            `strTableName` = strTableName,
            `strUniqueKey` = strUniqueKey,
            `strFieldName` = strFieldName,
            `strOldFieldValue` = strOldFieldValue,
            `strNewFieldValue` = strNewFieldValue;
END$$

/* 
	 tblSection Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUSection`;$$
Create Trigger `trgUSection` AFTER UPDATE On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.intSectionID <> New.intSectionID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intSectionID',Old.intSectionID,New.intSectionID);
	End If;
    
    If Old.intInstructorID <> New.intInstructorID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'intInstructorID',Old.intInstructorID,New.intInstructorID);
	End If;
    
    If Old.dtRegistrationCloses <> New.dtRegistrationCloses Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection',Old.intClassID,'dtRegistrationCloses',Old.dtRegistrationCloses,New.dtRegistrationCloses);
	End If;
End$$

Drop Trigger IF Exists `trgISection`;$$
Create Trigger `trgISection` AFTER INSERT On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intClassID',NULL,New.intClassID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intSectionID',NULL,New.intSectionID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection',New.intClassID,'dtRegistrationCloses',NULL,New.dtRegistrationCloses);
End$$

Drop Trigger IF Exists `trgDSection`;$$
Create Trigger `trgDSection` AFTER Delete On `tblSection`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intClassID',Old.intClassID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intSectionID',Old.intSectionID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection',Old.intClassID,'dtRegistrationCloses',Old.dtRegistrationCloses,Null);
End$$

/* 
	 tblInstructor Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUInstructor`;$$
Create Trigger `trgUInstructor` AFTER UPDATE On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    If Old.strFName <> New.strFName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblInstructor',Old.intInstructorID,'strFName',Old.strFName,New.strFName);
	End If;
    
    If Old.strLName <> New.strLName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblInstructor',Old.intInstructorID,'strLName',Old.strFName,New.strFName);
	End If;
End$$

Drop Trigger IF Exists `trgIInstructor`;$$
Create Trigger `trgIInstructor` AFTER INSERT On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'strFName',NULL,New.strFName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblInstructor',New.intInstructorID,'strLName',NULL,New.strLName);
End$$

Drop Trigger IF Exists `trgDInstructor`;$$
Create Trigger `trgDInstructor` AFTER Delete On `tblInstructor`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'strFName',Old.strFName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblInstructor',Old.intInstructorID,'strLName',Old.strLName,Null);
End$$

/* 
	 tblEnrollment Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUEnrollment`;$$
Create Trigger `trgUEnrollment` AFTER UPDATE On `tblEnrollment`
For Each Row Begin
    Declare dtNow DateTime;
    Set dtNow = Now();
<<<<<<< HEAD
    
    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollmentID',Old.intEnrollmentID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.intStudentID <> New.intStudentID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'intStudent',Old.intStudentID,New.intStudentID);
	End If;
    
=======
    -- Comment
>>>>>>> 79fcb129bfafe6a30766c0445072a35431dd6f22
    If Old.tsEnrollmentDate <> New.tsEnrollmentDate Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'tsEnrollmentDate',Old.tsEnrollmentDate,New.tsEnrollmentDate);
	End If;
    
    If Old.bolStatus <> New.bolStatus Then
		Call sp_tblAuditInsert(dtNow,'Update','tblEnrollment',Old.intEnrollmentID,'bolStatus',Old.bolStatus,New.bolStatus);
	End If;
End$$

Drop Trigger IF Exists `trgIEnrollment`;$$
Create Trigger `trgIEnrollment` AFTER INSERT On `tblEnrollment`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'intStudentID',NULL,New.intStudentID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'tsEnrollmentDate',NULL,New.tsEnrollmentDate);
    Call sp_tblAuditInsert(dtNow,'Insert','tblEnrollment',New.intEnrollmentID,'bolStatus',NULL,New.bolStatus);
End$$

Drop Trigger IF Exists `trgDEnrollment`;$$
Create Trigger `trgDEnrollment` AFTER Delete On `tblEnrollment`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'intStudentID',Old.intStudentID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'tsEnrollmentDate',Old.tsEnrollmentDate,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblEnrollment',Old.intEnrollmentID,'bolStatus',Old.bolStatus,Null);
End$$

/* 
	 tblCourse Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUCourse`;$$
Create Trigger `trgUCourse` AFTER UPDATE On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    
    If Old.strCourseName <> New.strCourseName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'strCourseName',Old.strCourseName,New.strCourseName);
	End If;
    
    If Old.strCourseDesc <> New.strCourseDesc Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'strCourseDesc',Old.strCourseDesc,New.strCourseDesc);
	End If;
    
    If Old.intCreditHours <> New.intCreditHours Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'intCreditHours',Old.intCreditHours,New.intCreditHours);
	End If;
    
    If Old.intInstructorID <> New.intInstructorID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'intInstructorID',Old.intInstructorID,New.intInstructorID);
	End If;
    
    If Old.bolAllowToEnroll <> New.bolAllowToEnroll Then
		Call sp_tblAuditInsert(dtNow,'Update','tblCourse',Old.intCourseID,'bolAllowToEnroll',Old.bolAllowToEnroll,New.bolAllowToEnroll);
	End If;
End$$

Drop Trigger IF Exists `trgICourse`;$$
Create Trigger `trgICourse` AFTER INSERT On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'strCourseName',NULL,New.strCourseName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'strCourseDesc',NULL,New.strCourseDesc);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intCreditHours',NULL,New.intCreditHours);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'intInstructorID',NULL,New.intInstructorID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblCourse',New.intCourseID,'bolAllowToEnroll',NULL,New.bolAllowToEnroll);
End$$

Drop Trigger IF Exists `trgDCourse`;$$
Create Trigger `trgDCourse` AFTER Delete On `tblCourse`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'strCourseName',Old.strCourseName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'strCourseDesc',Old.strCourseDesc,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intCreditHours',Old.intCreditHours,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'intInstructorID',Old.intInstructorID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblCourse',Old.intCourseID,'bolAllowToEnroll',Old.bolAllowToEnroll,Null);
End$$

/* 
	 tblStudent Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUStudent`;$$
Create Trigger `trgUStudent` AFTER UPDATE On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	
    If Old.strFName <> New.strFName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblStudent',Old.intStudentID,'strFName',Old.strFName,New.strFName);
	End If;
    
    If Old.strLName <> New.strLName Then
		Call sp_tblAuditInsert(dtNow,'Update','tblStudent',Old.intStudentID,'strLName',Old.strFName,New.strFName);
	End If;
End$$

Drop Trigger IF Exists `trgIStudent`;$$
Create Trigger `trgIStudent` AFTER INSERT On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'intStudentID',NULL,New.intStudentID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'strFName',NULL,New.strFName);
    Call sp_tblAuditInsert(dtNow,'Insert','tblStudent',New.intStudentID,'strLName',NULL,New.strLName);
End$$

Drop Trigger IF Exists `trgDStudent`;$$
Create Trigger `trgDStudent` AFTER Delete On `tblStudent`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'intStudentID',Old.intStudentID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'strFName',Old.strFName,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblStudent',Old.intStudentID,'strLName',Old.strLName,Null);
End$$

/* 
	 tblSection_Updated Trigger Section
		Update | Insert | Delete
*/

Drop Trigger IF Exists `trgUSection_Updated`;$$
Create Trigger `trgUSection_Updated` AFTER UPDATE On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();

    If Old.intCourseID <> New.intCourseID Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection_Updated',Old.intSectionID,'intCourseID',Old.intCourseID,New.intCourseID);
	End If;
    
    If Old.strSectionDesc <> New.strSectionDesc Then
		Call sp_tblAuditInsert(dtNow,'Update','tblSection_Updated',Old.intSectionID,'strSectionDesc',Old.strSectionDesc,New.strSectionDesc);
	End If;
End$$

Drop Trigger IF Exists `trgISection_Updated`;$$
Create Trigger `trgISection_Updated` AFTER INSERT On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'intSectionID',NULL,New.intSectionID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'intCourseID',NULL,New.intCourseID);
    Call sp_tblAuditInsert(dtNow,'Insert','tblSection_Updated',New.intSectionID,'strSectionDesc',NULL,New.strSectionDesc);
    
End$$

Drop Trigger IF Exists `trgDSection_Updated`;$$
Create Trigger `trgDSection_Updated` AFTER Delete On `tblSection_Updated`
For Each Row Begin
	Declare dtNow DateTime;
    Set dtNow = Now();
	Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'intSectionID',Old.intSectionID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'intCourseID',Old.intCourseID,Null);
    Call sp_tblAuditInsert(dtNow,'Delete','tblSection_Updated',Old.intSectionID,'strSectionDesc',Old.strSectionDesc,Null);
End$$

Delimiter ;