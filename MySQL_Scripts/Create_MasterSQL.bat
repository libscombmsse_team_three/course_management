@ECHO OFF
REM *** Script for Creating a Master SQL file
REM
REM
IF EXIST Master_Database_Objects_Script.sql (DEL Master_Database_Objects_Script.sql)
ECHO Adding Use Statement
IF EXIST USE_Statement.sql (type USE_Statement.sql > Master_Database_Objects_Script.sql)
ECHO. >> Master_Database_Objects_Script.sql
ECHO. >> Master_Database_Objects_Script.sql
ECHO Inserting Create Table SQL
for %%f in (Create_Tables\*.sql) do (
	IF EXIST %%f (type %%f >> Master_Database_Objects_Script.sql)
	ECHO. >> Master_Database_Objects_Script.sql
	ECHO. >> Master_Database_Objects_Script.sql
)
ECHO Inserting Audit Objects
for %%f in (AuditObjects\*.sql) do (
	IF EXIST %%f (type %%f >> Master_Database_Objects_Script.sql)
	ECHO. >> Master_Database_Objects_Script.sql
	ECHO. >> Master_Database_Objects_Script.sql
)
ECHO Inserting Front_End_SPs
for %%f in (Front_End_SPs\*.sql) do (
	IF EXIST %%f (type %%f >> Master_Database_Objects_Script.sql)
	ECHO. >> Master_Database_Objects_Script.sql
	ECHO. >> Master_Database_Objects_Script.sql
)