Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Update;$$
CREATE PROCEDURE sp_tblCourse_Update(
IN pintCourseID INT(11),
IN pstrCourseName VARCHAR(50),
IN pstrCourseDesc VARCHAR(150),
IN pintCreditHours  INT(11),
IN pbolAllowEnroll TINYINT(1),
IN pintInstructorID INT(11)
)
BEGIN
  UPDATE Lipscomb.tblCourse
  SET
    strCourseName = pstrCourseName,
    strCourseDesc =pstrCourseDesc,
    intCreditHours = pintCreditHours,
    bolAllowEnroll=pbolAllowEnroll,
    intInstructorID=pintInstructorID
  WHERE 
    intCourseID = pintCourseID;
      
	select "Success";
    
END$$

Delimiter ;






