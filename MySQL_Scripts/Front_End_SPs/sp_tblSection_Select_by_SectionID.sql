Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_SectionID;$$
CREATE PROCEDURE sp_tblSection_Select_by_SectionID (IN pintSectionID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intSectionID=pintSectionID;

    
END$$

Delimiter ;

