Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_StudentID;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_StudentID (IN pintStudentID INT(11))
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where intStudentID=pintStudentID;

    
END$$

Delimiter ;

