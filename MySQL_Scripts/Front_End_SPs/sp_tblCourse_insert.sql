Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Insert;$$
CREATE PROCEDURE sp_tblCourse_Insert(
IN pstrCourseName VARCHAR(50),
IN pstrCourseDesc VARCHAR(150),
IN pintCreditHours  INT(11) 
)
BEGIN
    INSERT INTO Lipscomb.tblCourse(strCourseName,strCourseDesc,intCreditHours)
      VALUES(pstrCourseName,pstrCourseDesc,pintCreditHours);
      
	select "Success";
    
END$$

Delimiter ;