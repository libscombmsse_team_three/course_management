Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_All;$$
CREATE PROCEDURE sp_tblEnrollment_Select_All ()
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment;

    
END$$

Delimiter ;

