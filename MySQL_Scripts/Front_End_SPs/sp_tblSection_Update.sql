Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Update;$$
CREATE PROCEDURE sp_tblSection_Update(
IN pintClassID INT(11),
IN pintCourseID INT(11),
IN pintSectionID INT(11), 
IN pintInstructorID INT(11),
IN pdtRegistrationCloses DATETIME
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblSection SET 
      intCourseID =pintCourseID,
      intSectionID = pintSectionID,
      intInstructorID = pintInstructorID,
      dtRegistrationCloses = pdtRegistrationCloses
    WHERE intClassID = pintClassID;

	select strResponse;
    
END$$

Delimiter ;

