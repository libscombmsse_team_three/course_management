Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Select_by_InstructorID;$$
CREATE PROCEDURE sp_tblInstructor_Select_by_InstructorID (IN pintInstructorID INT(11))
BEGIN
  SELECT 
    intInstructorID,
    strFName,
    strLName
  FROM tblInstructor
  where intInstructorID=pintInstructorID;

    
END$$

Delimiter ;

