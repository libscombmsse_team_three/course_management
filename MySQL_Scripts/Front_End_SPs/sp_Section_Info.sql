Delimiter $$
DROP procedure IF EXISTS sp_Section_Info;$$
CREATE PROCEDURE sp_Section_Info ()
BEGIN

  SELECT 
    tblSection.intClassID,
    tblSection.intCourseID,
    tblSection.intSectionID,
    tblSection.intInstructorID,
    tblSection.dtRegistrationCloses,
    tblInstructor.strFName,
    tblInstructor.strLName,
    tblCourse.strCourseName,
    tblCourse.strCourseDesc,
    tblCourse.intCreditHours
  FROM tblSection, tblInstructor, tblCourse
  where tblSection.intInstructorID=tblInstructor.intInstructorID 
        and tblSection.intCourseID=tblCourse.intCourseID;

    
END$$

Delimiter ;

