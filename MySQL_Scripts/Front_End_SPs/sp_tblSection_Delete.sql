Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Delete;$$
CREATE PROCEDURE sp_tblSection_Delete(
IN pintClassID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblSection
    WHERE intClassID = pintClassID;

	select strResponse;
    
END$$

Delimiter ;

