Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Delete;$$
CREATE PROCEDURE sp_tblCourse_Delete(
IN pintCourseID INT(11)

)
BEGIN
  DELETE FROM Lipscomb.tblCourse
  WHERE intCourseID = pintCourseID;

  select "Success";
    
END$$

Delimiter ;

