Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_All;$$
CREATE PROCEDURE sp_tblSection_Select_All ()
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection;

    
END$$

Delimiter ;

