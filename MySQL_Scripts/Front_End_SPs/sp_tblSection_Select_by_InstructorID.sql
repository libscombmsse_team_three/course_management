Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_InstructorID;$$
CREATE PROCEDURE sp_tblSection_Select_by_InstructorID (IN pintInstructorID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intInstructorID=pintInstructorID;

    
END$$

Delimiter ;

