Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Select_by_StudentID;$$
CREATE PROCEDURE sp_tblStudent_Select_by_StudentID (
IN pntStudentID INT(11)
)
BEGIN
    SELECT tblStudent.intStudentID,
           tblStudent.strFName,
           tblStudent.strLName
    FROM Lipscomb.tblStudent
    WHERE ntStudentID=pntStudentID;
    
END$$

Delimiter ;

