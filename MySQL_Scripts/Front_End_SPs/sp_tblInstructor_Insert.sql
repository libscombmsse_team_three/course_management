Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Insert;$$
CREATE PROCEDURE sp_tblInstructor_Insert(
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    INSERT INTO Lipscomb.tblInstructor(strFName,strLName)
    VALUES(pstrFName,pstrLName);
      
	select strResponse;
    
END$$

Delimiter ;

