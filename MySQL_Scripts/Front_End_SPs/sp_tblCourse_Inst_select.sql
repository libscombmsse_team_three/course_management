Delimiter $$
DROP procedure IF EXISTS sp_tblCourse_Inst_select;$$
CREATE PROCEDURE sp_tblCourse_Inst_select ()
BEGIN
  SELECT * FROM tblCourse c 
	INNER JOIN tblInstructor i 
		ON c.intInstructorID=i.intInstructorID
	ORDER BY c.intCourseID ASC;

    
END$$

Delimiter ;

