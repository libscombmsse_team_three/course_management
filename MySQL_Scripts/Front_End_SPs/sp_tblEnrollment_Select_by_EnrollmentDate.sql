Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_EnrollmentDate;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_EnrollmentDate (IN ptsEnrollmentDate DATETIME)
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where tsEnrollmentDate=ptsEnrollmentDate;

    
END$$

Delimiter ;

