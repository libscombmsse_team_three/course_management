Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Select_by_ClassID;$$
CREATE PROCEDURE sp_tblEnrollment_Select_by_ClassID (IN pintClassID INT(11))
BEGIN
  SELECT 
    intClassID,
    intStudentID,
    tsEnrollmentDate
  FROM tblEnrollment
  where intClassID=pintClassID;

    
END$$

Delimiter ;

