Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Delete;$$
CREATE PROCEDURE sp_tblInstructor_Delete(
IN pintInstructorID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblInstructor
    WHERE intInstructorID = pintInstructorID;

	select strResponse;
    
END$$

Delimiter ;

