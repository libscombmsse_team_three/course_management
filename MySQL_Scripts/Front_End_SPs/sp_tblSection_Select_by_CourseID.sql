Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Select_by_CourseID;$$
CREATE PROCEDURE sp_tblSection_Select_by_CourseID (IN pintCourseID int(11))
BEGIN
  SELECT 
    intClassID,
    intCourseID,
    intSectionID,
    intInstructorID,
    dtRegistrationCloses
  FROM tblSection
  where intCourseID=pintCourseID;

    
END$$

Delimiter ;

