Delimiter $$
DROP procedure IF EXISTS sp_tblSection_Insert;$$
CREATE PROCEDURE sp_tblSection_Insert(
IN pintCourseID INT(11),
IN pintSectionID INT(11), 
IN pintInstructorID INT(11) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    #intClassID auto increments & dtRegistrationCloses defaults to null
    INSERT INTO Lipscomb.tblSection(intCourseID,intSectionID,intInstructorID)
    VALUES(pintCourseID,pintSectionID,pintInstructorID);
      
	select strResponse;
    
END$$

Delimiter ;

