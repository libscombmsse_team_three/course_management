Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Delete;$$
CREATE PROCEDURE sp_tblEnrollment_Delete(
IN pintCourseID INT(11),
IN pintStudentID INT(11)
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

     DELETE FROM Lipscomb.tblEnrollment
     WHERE intCourseID = pintCourseID AND intStudentID = pintStudentID;

	select strResponse;
    
END$$

Delimiter ;

