Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Delete;$$
CREATE PROCEDURE sp_tblStudent_Delete(
IN pntStudentID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    DELETE FROM Lipscomb.tblStudent
    WHERE ntStudentID=pntStudentID;

	select strResponse;
    
END$$

Delimiter ;

