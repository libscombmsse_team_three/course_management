Delimiter $$
DROP procedure IF EXISTS sp_tblEnrollment_Insert;$$
CREATE PROCEDURE sp_tblEnrollment_Insert(
IN pintCourseID INT(11),
IN pintStudentID INT(11) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    INSERT INTO Lipscomb.tblEnrollment(intCourseID,intStudentID,tsEnrollmentDate)
      VALUES(pintCourseID,pintStudentID,NOW());
      
	select strResponse;
    
END$$

Delimiter ;

