Delimiter $$
DROP procedure IF EXISTS sp_tblInstructor_Update;$$
CREATE PROCEDURE sp_tblInstructor_Update(
IN pintInstructorID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblInstructor SET
      strFName = pstrFName,
      strLName = pstrLName
    WHERE `intInstructorID` = pintInstructorID;

      
	select strResponse;
    
END$$

Delimiter ;

