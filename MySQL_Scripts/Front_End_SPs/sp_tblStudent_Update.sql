Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Update;$$
CREATE PROCEDURE sp_tblStudent_Update(
IN pntStudentID INT(11),
IN pstrFName VARCHAR(25),
IN pstrLName VARCHAR(25) 
)
BEGIN
    DECLARE strResponse VARCHAR(50);
    set strResponse= "Success";

    UPDATE Lipscomb.tblStudent SET 
      strFName = pstrFName,
      strLName = pstrLName
    WHERE intStudentID =pntStudentID;

	select strResponse;
    
END$$

Delimiter ;

