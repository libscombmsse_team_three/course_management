Delimiter $$
DROP procedure IF EXISTS sp_tblStudent_Select_All;$$
CREATE PROCEDURE sp_tblStudent_Select_All ()
BEGIN
    SELECT tblStudent.intStudentID,
           tblStudent.strFName,
           tblStudent.strLName
    FROM Lipscomb.tblStudent;
    
END$$

Delimiter ;

