var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3001;

app.use(bodyParser.json());

// view engine setup
app.use(express.static(__dirname + '/public'));     //serve static assets

var studentService=require('./services/student.js');
var studentapi = require('./routes/student-api')(studentService);

app.use('/studentapi',studentapi);

var courseService=require('./services/course.js');
var courseapi = require('./routes/course-api')(courseService);
app.use('/courseapi',courseapi);

app.listen(port, function() {
    console.log('Listening on ' + port);
});
