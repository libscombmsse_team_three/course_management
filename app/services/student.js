var mysql = require('mysql');

var pool  = mysql.createPool({
  host     : 'mysql.khnt.us',
  user     : 'Lipscomb',
  password : 'College2015!',
  database : 'Lipscomb'
});

function Student(){
};

Student.prototype.getStudents=function(req,res,done){

	var sql="CALL sp_tblStudent_Select_All()";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Student.prototype.createStudent=function(req,res,done){
	
	var sql="CALL sp_tblStudent_Insert('"+req.body.firstName+"','"+ req.body.lastName+"')";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Student.prototype.updateStudent=function(req,res,done){

	var sql="CALL sp_tblStudent_Update("+req.body.id+",'"+req.body.firstName+"','"+ req.body.lastName+"')";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Student.prototype.deleteStudent=function(req,res,done){

	var sql="CALL sp_tblStudent_Delete("+req.params.studentId+",'','')";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};


Student.prototype.getStudentCourses=function(req,res,done){

	var sql="SELECT *,\
				EXISTS(SELECT * FROM tblEnrollment e \
					WHERE e.intCourseID=c.intCourseID AND e.intStudentID="+ req.params.studentId+") AS isEnrolled \
				FROM tblCourse c \
					INNER JOIN tblInstructor i \
						ON c.intInstructorID=i.intInstructorID \
					ORDER BY c.intCourseID ASC";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Student.prototype.createEnrollment=function(req,res,done){

	var sql="CALL sp_tblEnrollment_Insert("+req.params.courseId+","+req.params.studentId+")";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Student.prototype.deleteEnrollment=function(req,res,done){

	var sql="CALL sp_tblEnrollment_Delete("+req.params.courseId+","+req.params.studentId+")";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

module.exports = new Student();