var mysql = require('mysql');

var pool  = mysql.createPool({
  host     : 'mysql.khnt.us',
  user     : 'Lipscomb',
  password : 'College2015!',
  database : 'Lipscomb'
});

function Course(){
};

Course.prototype.getInstructors=function(req,res,done){

	var sql="CALL sp_tblInstructor_Select_All()";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Course.prototype.createCourse=function(req,res,done){

	pool.getConnection(function(err, connection) {
		  connection.query("INSERT INTO tblCourse\
		  					(strCourseName,strCourseDesc,intCreditHours,bolAllowToEnroll,intInstructorID)\
		  						VALUES('"+req.body.name+"','"+req.body.description+"',"
		  							+req.body.creditHours+",true,"+req.body.instructorId+");"
				, function(err, rows) {
		   
				    if (err){
					  	console.log(err);
					  	connection.release();
					  	return done(err,rows);
					}

					var courseId=rows.insertId;

					req.body.courseId=courseId;

					for(var index in req.body.sections){
						connection.query("INSERT INTO tblSection_Updated\
											(intCourseID,strSectionDesc)\
											VALUES("+courseId+",'"+ req.body.sections[index] +"');"

										, function(err, rows) {

											if (err){
											  	console.log(err);
											}
									});
					}

					connection.release();
					return done(null,req.body);
		  		});
	});
};

Course.prototype.getAllCourses=function(req,res,done){

	var sql="SELECT * FROM tblCourse c INNER JOIN tblInstructor i \
				ON c.intInstructorID=i.intInstructorID \
				ORDER BY c.intCourseID ASC";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};

Course.prototype.getCourseSections=function(req,res,done){

	var sql="SELECT * FROM tblSection_Updated \
				WHERE intCourseID="+ req.params.courseId+" \
				ORDER BY intSectionID ASC";

	pool.getConnection(function(err, connection) {
		  connection.query(sql, function(err, rows) {
		   
			    if (err){
				  	console.log(err);
				  	connection.release();
				  	return done(err,rows);
				}

				connection.release();
				return done(null,rows);
		  });
	});
};
module.exports = new Course();