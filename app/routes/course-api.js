var express = require('express');
var router = express.Router();

module.exports = function(courseService){

	router
		.route("/courses/instructors")
			.get(function(req,res){
				
				courseService.getInstructors(req,null,function(err,instructors){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:instructors});
				});
			});

	router
		.route("/courses")
			.post(function(req,res){
				
				courseService.createCourse(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/courses")
			.get(function(req,res){
				
				courseService.getAllCourses(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/courses/:courseId/sections")
			.get(function(req,res){
				
				courseService.getCourseSections(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	return router;
};