var express = require('express');
var router = express.Router();

module.exports = function(studentService){

	router
		.route("/students")
			.get(function(req,res){
				
				studentService.getStudents(req,null,function(err,students){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:students});
				});
			});

	router
		.route("/students")
			.post(function(req,res){
				
				studentService.createStudent(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/students/:studentId")
			.put(function(req,res){
				
				studentService.updateStudent(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/students/:studentId")
			.delete(function(req,res){
				
				studentService.deleteStudent(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/students/:studentId/courses")
			.get(function(req,res){
				
				studentService.getStudentCourses(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/students/:studentId/courses/:courseId")
			.post(function(req,res){
				
				studentService.createEnrollment(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	router
		.route("/students/:studentId/courses/:courseId")
			.delete(function(req,res){
				
				studentService.deleteEnrollment(req,null,function(err,result){

					if(err){
		                res.send({status:"failed",error:err});
		            }

		            res.send({status:"success",result:result});
				});
			});

	return router;
};