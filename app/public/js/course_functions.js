$(document).ready(function()
{
    var loadCourses=function(){

        $.ajax({
            type: "GET",
            url: "courseapi/courses",
            success: function(data){
                $('#course-tbody').empty();
                // next, use course ids to get course data from Course table
                $.each(data.result, function(index, course){

                    var data_description = "the course's description"; // change to course_data.result[index].courseDescription
                    $('#course-table').append('<tr>'+
                                    /*'<td><input type="checkbox" class="checkthis" /></td>'+*/
                                    '<td id="td2-course_id'+course.intCourseID+'">'+course.intCourseID+'</td>'+
                                    '<td>'+course.strCourseName+'</td>'+
                                    '<td>'+course.intCreditHours+'</td>'+
                                    '<td>'+course.strFName + ' '+ course.strLName+'</td>'+
                                    /*'<td id="td2-location'+index+'">CCT 102</td>'+*/
                                    '<td><p data-placement="top" data-toggle="tooltip" title="'+course.strCourseDesc+'"><button class="btn btn-info btn-xs"><span id="description-item'+course.intCourseID+'" class="glyphicon glyphicon-comment" rel="popover"></span></button><script>'+
                                                '$(document).ready(function() {'+
                                                    '$("#description-item'+course.intCourseID+'").popover({'+
                                                        'animation: false,'+
                                                        'content: "'+course.strCourseDesc+'",'+
                                                        'placement: "top"'+
                                                    '});'+
                                                '});'+
                                            '</script></p></td>'+
                                    '<td><button class="btn btn-info btn-xs" type="button" data-toggle="modal" data-target="#course-sections-modal" data-courseId='+ course.intCourseID +'><span class="glyphicon glyphicon-tasks"></span></button></td>'+
                                '</tr>');
                        });
                    },
                    error: function(e){
                        alert(e.message);
                    }
                });
    }

    loadCourses();

    $('#course-search-btn').click(function()
    {
        loadCourses();
    });

    // checkboxes button
    $("#course-table #checkall").click(function()
    {
        if ($("#course-table #checkall").is(':checked'))
        {
            $("#course-table input[type=checkbox]").each(function()
            {
                $(this).prop("checked", true);
            });

        } 
        else 
        {
            $("#course-table input[type=checkbox]").each(function()
            {
                $(this).prop("checked", false);
            });
        }
    });
    
    $("[data-toggle=tooltip]").tooltip();

    $('#add-course-btn').click(function()
    {
        var course={
            name:$('#add-course-cn').val(),
            description:$('#add-course-desc').val(),
            creditHours:$('#add-course-ch').val(),
            instructorId:$('#instructorPicklist').val(),
            sections:[]
        };

        $("tr.item td:first-child").each(function(i,td) {
            $td = $(td);
            course.sections.push($td.html());
        });

        console.log(course);

        $.ajax(
        {
            type: "POST",
            url: "courseapi/courses",
            dataType:"json",
            data: JSON.stringify(course),
            contentType:"application/json",
            success: function()
            {
                alert("Successfully added!");
                $("#new-course-modal").modal("hide");
                loadCourses();
            },
            error: function(e)
            {
                alert(e.message);
            }
        });
    });

    $("#course-section-add-btn").click(function(){
        $('#course-section-table')
            .append(
                '<tr class="item">'+
                    '<td>'+$('#course-section-sn').val()+'</td>'+
                    '<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button id="delete-section-table-btn" class="btn btn-danger btn-xs delete-section-btn" type="button"><span class="glyphicon glyphicon-trash"></span></button></p></td>'+
                '</tr>');

        $('#course-section-sn').val('');
    });

    $(document).on("click","#delete-section-table-btn",function()
    {
        $(this).closest('tr').remove();
    });

    $('#new-course-modal').on('shown.bs.modal', function () {
        $.ajax(
        {
            type: "GET",
            url: "courseapi/courses/instructors",
            success: function(data) 
            {
                var instructorPicklistNode=$("#instructorPicklist");
                instructorPicklistNode.empty();
                instructorPicklistNode.append('<option value="">Select Instructor</option>');
                $.each(data.result[0], function(index, instructor) 
                {
                   instructorPicklistNode.append('<option value="'+ instructor.intInstructorID +'">'+ instructor.strFName + ' '+ instructor.strLName +'</option>');
                });
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    });

    /*$(document).on("click","#btnShowCourses",function(e)
    {
         console.log(e);

        $("#studentCourses").dialog({
            width:900,show:true
        });
    });*/

    $('#course-sections-modal').on('shown.bs.modal', function (e) {

        var courseId=$(e.relatedTarget).attr('data-courseId');
        
        var url="courseapi/courses/"+courseId+"/sections";

        var sectionListNode=$("#course-sections-list");
        sectionListNode.empty();

        $.ajax(
        {
            type: "GET",
            url: url,
            success: function(data) 
            {
                $.each(data.result, function(index, section) 
                {
                   sectionListNode.append('<li class="list-group-item">'+ section.strSectionDesc+'</li>');
                });
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    });
});