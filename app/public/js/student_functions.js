$(document).ready(function()
{
    var url = "";

    var loadStudents=function(){
        // clear the student-table
        $('#student-tbody').empty();
       
        $.ajax(
        {
            type: "GET",
            url: "studentapi/students",
            success: function(data) 
            {
                var student_id;
                $.each(data.result[0], function(index, student) 
                {
                    // NOTE: change from index to data.id or data.studentId
                    student_id = index;
                    $('#student-table').append('<tr id="'+student.intStudentID+'">'+
                                            /*'<td><input type="checkbox" class="checkthis" /></td>'+*/
                                            '<td>'+student.strFName+'</td>'+
                                            '<td>'+student.strLName+'</td>'+
                                            '<td>'+student.intStudentID+'</td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Courses"><button id="btnShowCourses" data-studentId="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-warning btn-xs view-courses-btn" data-title="Courses" data-toggle="modal" data-target="#courses"><span class="glyphicon glyphicon-list"></span></button></p></td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button data-id="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-primary btn-xs edit-student-btn" data-title="Edit" data-toggle="modal" data-target="#student-edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button id="delete-student-btn" data-id="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-danger btn-xs delete-student-btn" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>'+
                                        '</tr>');

                    $("#"+student.intStudentID).data("id",student.intStudentID);
                    $("#"+student.intStudentID).data("firstName",student.strFName);
                    $("#"+student.intStudentID).data("lastName",student.strLName);
                });
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    }

    loadStudents();

    // search button (for students)
    $('#student-search-btn').click(function()
    {
        // clear the student-table
        $('#student-tbody').empty();
        
        // determine the url based on text in fields (firstname,lastname,id)
        if ($('#sfn').val() || $('#sln').val() || $('#sid').val())
        {
            // search by...
            url = "studentapi/students"
        }
        else
        {
            alert("Please enter Firstname, Lastname, and/or Student ID.");
            return;
        }

        $.ajax(
        {
        	type: "GET",
            url: "studentapi/students",
            success: function(data) 
            {
                console.log(data);

                var student_id;
            	$.each(data.result[0], function(index, student) 
             	{
                    // NOTE: change from index to data.id or data.studentId
                    student_id = index;
                    $('#student-table').append('<tr id="'+student.intStudentID+'">'+
                                            /*'<td><input type="checkbox" class="checkthis" /></td>'+*/
                                            '<td>'+student.strFName+'</td>'+
                                            '<td>'+student.strLName+'</td>'+
                                            '<td>'+student.intStudentID+'</td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Courses"><button data-id="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-warning btn-xs view-courses-btn" data-title="Courses" data-toggle="modal" data-target="#courses" ><span class="glyphicon glyphicon-list"></span></button></p></td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button data-id="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-primary btn-xs edit-student-btn" data-title="Edit" data-toggle="modal" data-target="#student-edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>'+
                                            '<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button id="delete-student-btn" data-id="'+student.intStudentID+'" data-index="'+index+'" class="btn btn-danger btn-xs delete-student-btn" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>'+
                                        '</tr>');

                    $("#"+student.intStudentID).data("id",student.intStudentID);
                    $("#"+student.intStudentID).data("firstName",student.strFName);
                    $("#"+student.intStudentID).data("lastName",student.strLName);
                });
            },
            error: function(e) 
            {
            	alert(e.message);
            }
        });

    });

    // checkboxes button
    $("#student-table #checkall").click(function()
    {
        if ($("#student-table #checkall").is(':checked'))
        {
            $("#student-table input[type=checkbox]").each(function()
            {
                $(this).prop("checked", true);
            });

        } 
        else 
        {
            $("#student-table input[type=checkbox]").each(function()
            {
                $(this).prop("checked", false);
            });
        }
    });
    
    $("[data-toggle=tooltip]").tooltip();

    // add student button
    $('#add-student-btn').click(function()
    {
        // need format to pass parameters appropriately
        
        if (!$('#add-student-fn').val() && !$('#add-student-ln').val() && !$('#add-student-id').val())
        {
            alert("Please fill in all fields.");
            return;
        }

        var student={
            firstName:$('#add-student-fn').val(),
            lastName:$('#add-student-ln').val()
        };

        console.log(student);

        $.ajax(
        {
            type: "POST",
            url: "studentapi/students",
            dataType:"json",
            data: JSON.stringify(student),
            contentType:"application/json",
            success: function()
            {
                alert("Successfully added!");
                $("#new-student-modal").modal("hide");
                loadStudents();
            },
            error: function(e)
            {
                alert(e.message);
            }
        });

    });

    // view student courses button
    $(document).on("click",".view-courses-btn",function()
    {
        // clear student courses table
        $('#student-courses-tbody').empty();

        var student_id = $(this).data('id');
        // store the student id in the enroll button as a data-id attribute
        $('#enroll-btn').data.id = student_id;
        
        // since the db isn't designed to easily access all course info,
        // first, get list of class ids using student id
        $.ajax(
        {
            type: "GET",
            url: "studentapi/students",
            //data: student_id,
            success: function(class_data) 
            {
                // next, get section data using class ids
                //var course_id;
                $.each(class_data.result, function(index,class_results)
                {
                    $.ajax(
                    {
                        type: "GET",
                        url: "studentapi/students",
                        //data: class_results[index].classId,
                        success: function(section_data)
                        {
                            // finally, get a list of course info using course ids
                            $.ajax(
                            {
                                type: "GET",
                                url: "studentapi/students",
                                //data: section_data.result[index].courseId,
                                success: function(course_data)
                                {
                                    var course_id = "-1"; // change to course_data.result[index].courseId or whatever it is
                                    var course_name = "Software Maintenance"; // change to course_data.result[index].courseName..
                                    var credit_hours = "3"; // change to course_data.result[index].creditHours...
                                    $('#student-courses-table').append('<tr>'+
                                                '<td id="td-coursename'+index+'">'+course_name+'</td>'+
                                                '<td id="td-courseid'+index+'">'+course_id+'</td>'+
                                                '<td id="td-sectionid'+index+'">'+section_data.result[index].firstName+'</td>'+
                                                '<td id="td-credithours'+index+'">'+credit_hours+'</td>'+
                                                '<td id="td-instructor'+index+'">William Senn</td>'+
                                                '<td id="td-location'+index+'">CCT 102</td>'+
                                                '<td><p data-placement="top" data-toggle="tooltip" title="Unenroll"><button data-id="'+student_id+'" data-courseid="'+course_id+'" class="btn btn-danger btn-xs unenroll-btn" data-title="Unenroll" data-toggle="modal" data-target="#course-unenroll" ><span class="glyphicon glyphicon-trash"></span></button></p></td>'+
                                            '</tr>');
                                }
                            });
                            
                            // get instructor name
                            $.ajax(
                            {
                                type: "GET",
                                url: "studentapi/students",
                                //data: section_data.instructorId,
                                success: function(instructor_data)
                                {
                                    $('#td-instructor'+index).html('instructor'+index); // .html(instructor_data.result[index].firstName+' '+instructor_data.lastName);
                                }
                            });
                        }
                    });
                });
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    });
    
    // enroll student in course(s)
    $('#enroll-btn').click(function()
    {
        // need format of parameters to correctly post data
        var data;

        // get student id from button data-id attribute (see above function)
        var student_id = $(this).data.id;
        data=student_id;

        if ($('#enroll-id1').val() || $('#enroll-id2').val() || $('#enroll-id3').val())
        {
            if ($('#enroll-id1').val())
            {
                // TODO: append correctly to data;
                data+="&"+$('#enroll-id1').val();
            }

            if ($('#enroll-id2').val())
            {
                // TODO: append correctly to data;
                data+="&"+$('#enroll-id2').val();
            }

            if ($('#enroll-id3').val())
            {
                // TODO: append correctly to data;
                data+="&"+$('#enroll-id3').val();
            }

        }
        else
        {
            alert("Please enter a Course ID to enroll in a course.");
            return;
        }

        // enroll student based on student id and course ids
        $.ajax(
        {
            type: "POST",
            url: "studentapi/students",
            //data: student_id,
            success: function(data) 
            {
                alert("Succesfully Enrolled!");
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });

    });

    // edit student information
    $(document).on("click",".edit-student-btn",function()
    {
        // get student info based on student id
        var currentRow = $(this).parent().parent().parent(); //tr 

        $('#edit-id').val(currentRow.data("id"));
        $('#edit-fn').val(currentRow.data("firstName"));
        $('#edit-ln').val(currentRow.data("lastName"));

    });

    // update student information
    $('#update-student-btn').click(function()
    {
        // need format of parameters to correctly post data

        var student={
            id:$('#edit-id').val(),
            firstName:$('#edit-fn').val(),
            lastName:$('#edit-ln').val()
        };

        if ($('#edit-fn').val() && $('#edit-ln').val())
        {
            //data+=$('#edit-fn').val() + "&" + $('#edit-ln').val();
        }
        else
        {
            alert("Please fill in both Firstname and Lastname fields.");
            return;
        }

        var putUrl="studentapi/students/"+student.id;

        $.ajax(
        {
            type: "PUT",
            url: putUrl,
            dataType:"json",
            data: JSON.stringify(student),
            contentType:"application/json",
            success: function()
            {
                alert("Successfully Updated!");
                $("#student-edit").modal("hide");
                loadStudents();
            },
            error: function(e)
            {
                alert(e.message);
            }
        });
    });

    // view unenroll button
    $(document).on("click","course-unenroll", function()
    {
        // store student id in confirm delete button data-id attribute
        $('#confirm-unenroll-btn').data.id = $(this).data('id');
        $('#confirm-unenroll-btn').data('courseid') = $(this).data('courseid');
    });

    // confirm unenroll student from course
    $('#confirm-unenroll-btn').click(function()
    {
        // unenroll student
        UnenrollStudent(0,0,$(this).data('id'),$(this).data('courseid'));   
    });

    function UnenrollStudent(index, course_data, student_id, course_id)
    {
        // delete records from enrollment table based on student id
        $.ajax(
        {
            type: "POST",
            url: "studentapi/students",
            //data: student_id,
            success: function(data) 
            {
                alert("Student "+student_id+" successfully unenrolled from"+course_id+".");
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    }

    // show delete student record btn
    $(document).on("click","#delete-student-btn",function()
    {
        var currentRow = $(this).parent().parent().parent(); //tr 

        // store student id in confirm delete button data-id attribute
        $('#confirm-delete-btn').data.id = currentRow.data("id");
    });

    // confirm delete student record btn
    $('#confirm-delete-btn').click(function()
    {
        var deleteUrl="studentapi/students/"+$(this).data.id;

        $.ajax(
        {
            type: "DELETE",
            url: deleteUrl,
            success: function()
            {
                alert("Student deleted successfully.!");
                $("#delete").modal("hide");
                loadStudents();
            },
            error: function(e)
            {
                alert(e.message);
            }
        });
    });

    // show delete student record btn
    $(document).on("click","#btnStudentEnroll",function()
    {
        var courseId=$(this).attr('data-courseId');
        var studentId=$(this).attr('data-studentId');

        var enrollmentUrl="studentapi/students/"+studentId+"/courses/"+courseId;

        $.ajax(
        {
            type: "POST",
            url: enrollmentUrl,
            success: function()
            {
                alert("Enrolled successfully.!");
                $("#courses").modal('hide');
            },
            error: function(e)
            {
                alert(e.message);
            }
        });
    });

    $(document).on("click","#btnStudentEnrollmentDrop",function()
    {
        var courseId=$(this).attr('data-courseId');
        var studentId=$(this).attr('data-studentId');

        var enrollmentDropUrl="studentapi/students/"+studentId+"/courses/"+courseId;

        $.ajax(
        {
            type: "DELETE",
            url: enrollmentDropUrl,
            success: function()
            {
                alert("Enrollment Dropped successfully.!");
                $("#courses").modal('hide');
            },
            error: function(e)
            {
                alert(e.message);
            }
        });
    });

    $('#courses').on('shown.bs.modal', function (e) {

        var studentId=$(e.relatedTarget).attr('data-studentId');
        
        var url="studentapi/students/"+studentId+"/courses";

        var studentEnrolledCourseTbody=$("#student-enrolled-course-tbody");
        studentEnrolledCourseTbody.empty();

        var studentAvailableCourseTbody=$("#student-available-course-tbody");
        studentAvailableCourseTbody.empty();

        $.ajax(
        {
            type: "GET",
            url: url,
            success: function(data) 
            {

                var enrolledCourses=data.result.filter(function(c){
                    return c.isEnrolled==1;
                });

                $.each(enrolledCourses, function(index, course){
                    studentEnrolledCourseTbody.append('<tr>'+
                        '<td id="td2-course_id'+course.intCourseID+'">'+course.intCourseID+'</td>'+
                        '<td>'+course.strCourseName+'</td>'+
                        '<td>'+course.intCreditHours+'</td>'+
                        '<td>'+course.strFName + ' '+ course.strLName+'</td>'+
                        /*'<td><p data-placement="top" data-toggle="tooltip" title="'+course.strCourseDesc+'"><button class="btn btn-info btn-xs"><span id="description-item'+course.intCourseID+'" class="glyphicon glyphicon-comment" rel="popover"></span></button><script>'+
                                    '$(document).ready(function() {'+
                                        '$("#description-item'+course.intCourseID+'").popover({'+
                                            'animation: false,'+
                                            'content: "'+course.strCourseDesc+'",'+
                                            'placement: "top"'+
                                        '});'+
                                    '});'+
                                '</script></p></td>'+*/
                        '<td>'+course.strCourseDesc+'</td>'+
                        '<td><button class="btn btn-info btn-xs" type="button" data-toggle="modal" data-target="#course-sections-modal" data-courseId='+ course.intCourseID +'><span class="glyphicon glyphicon-tasks"></span></button></td>'+
                        '<td><button class="btn btn-danger btn-xs" type="button" data-studentId='+studentId+' data-courseId='+ course.intCourseID +' id="btnStudentEnrollmentDrop"><span class="glyphicon glyphicon-education"></span></button></td>'+
                    '</tr>');
                });

                var availableCourses=data.result.filter(function(c){
                    return c.isEnrolled==0;
                });

                $.each(availableCourses, function(index, course){
                    studentAvailableCourseTbody.append('<tr>'+
                        '<td id="td2-course_id'+course.intCourseID+'">'+course.intCourseID+'</td>'+
                        '<td>'+course.strCourseName+'</td>'+
                        '<td>'+course.intCreditHours+'</td>'+
                        '<td>'+course.strFName + ' '+ course.strLName+'</td>'+
                        /*'<td><p data-placement="top" data-toggle="tooltip" title="'+course.strCourseDesc+'"><button class="btn btn-info btn-xs"><span id="description-item'+course.intCourseID+'" class="glyphicon glyphicon-comment" rel="popover"></span></button><script>'+
                                    '$(document).ready(function() {'+
                                        '$("#description-item'+course.intCourseID+'").popover({'+
                                            'animation: false,'+
                                            'content: "'+course.strCourseDesc+'",'+
                                            'placement: "top"'+
                                        '});'+
                                    '});'+
                                '</script></p></td>'+*/
                        '<td>'+course.strCourseDesc+'</td>'+
                        '<td><button class="btn btn-info btn-xs" type="button" data-toggle="modal" data-target="#course-sections-modal" data-courseId='+ course.intCourseID +'><span class="glyphicon glyphicon-tasks"></span></button></td>'+
                        '<td><button class="btn btn-success btn-xs" type="button" data-studentId='+studentId+' data-courseId='+ course.intCourseID +' id="btnStudentEnroll"><span class="glyphicon glyphicon-education"></span></button></td>'+
                    '</tr>');
                });
            },
            error: function(e) 
            {
                alert(e.message);
            }
        });
    });
});